#include "OPT3101.h" // library ver 1.0.1
#include <Wire.h>

OPT3101 sensor;

const long calibrationPeriod = 3000; // 캘리브레이션 기간을 3초로 설정
unsigned long calibrationStartTime;
bool calibrationDone = false;
uint16_t amplitudes[3] = {0};
int16_t distances[3] = {0};
long averageDistances[3] = {0};
long sampleCounts[3] = {0};

void setup()
{
    Serial.begin(115200);
    Wire.begin(25, 33, 100000); // SDA, SCL 핀 번호와 I2C 속도를 설정

    while (!Serial) {} // 시리얼 포트가 열릴 때까지 대기

    sensor.init();
    if (sensor.getLastError()) {
        Serial.print(F("Failed to initialize OPT3101: error "));
        Serial.println(sensor.getLastError());
        while (1) {}
    }

    sensor.setFrameTiming(256);
    sensor.setChannel(0);
    sensor.setBrightness(OPT3101Brightness::Adaptive);

    sensor.startSample();
    calibrationStartTime = millis(); // 캘리브레이션 시작 시간을 기록
}

void loop()
{
    if (sensor.isSampleDone()) {
        sensor.readOutputRegs();

        // 캘리브레이션 기간 동안 데이터 수집
        if (!calibrationDone && millis() - calibrationStartTime < calibrationPeriod) {
            sampleCounts[sensor.channelUsed]++;
            averageDistances[sensor.channelUsed] += sensor.distanceMillimeters;
        } else if (!calibrationDone) { // 캘리브레이션 종료
            calibrationDone = true;
            for (int i = 0; i < 3; i++) {
                if (sampleCounts[i] > 0) {
                    averageDistances[i] /= sampleCounts[i]; // 평균 거리 계산
                }
            }
            Serial.println(F("Calibration done."));
        } else { // 캘리브레이션 후 데이터 처리
            int16_t distance = sensor.distanceMillimeters;
            int16_t diff = distance - averageDistances[sensor.channelUsed];

            if (abs(diff) >= 100) {
                Serial.println(F("Significant difference detected."));
            } else {
                Serial.println(F("No significant difference."));
            }
        }

        // 다음 샘플을 위해 준비
        sensor.nextChannel();
        sensor.startSample();
    }
}

# OPT3101_Advanced

거리 센서 / 근접 센서

방식 : 적외선(IR) 센서

Arduino library : OPT3101 by Pololu 

version :  1.0.1

Interface : I2C, SDA(21), SCL(22)

I2C frequency : 100kHz

TX : 0, 1, 2

RX : 0